package br.com.alura.minhasmusicas.modelos;

public class Podcast extends Audio{

    private String aprentador;
    private String descricao;

    public String getAprentador() {
        return aprentador;
    }

    public void setAprentador(String aprentador) {
        this.aprentador = aprentador;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public int getClassificacao() {
        if(this.getTotalCurtidas() > 500){
            return 10;
        } else {
            return 8;
        }
    }
}
